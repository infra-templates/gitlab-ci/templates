image:
  name: hashicorp/terraform:1.0.0
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Default output file for Terraform plan
variables:
  PLAN: plan.tfplan
  JSON_PLAN_FILE: tfplan.json
  TF_IN_AUTOMATION: "true"

cache:
  key: "$CI_COMMIT_SHA"
  paths:
    - .terraform

.install-curl-jq: &install-curl-jq
  - apk add --update curl jq
  - alias convert_report="jq -r '([.resource_changes[].change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"

stages:
  - validate
  - plan
  - apply
  - deploy
  - destroy

validate:
  stage: validate
  script:
    - *install-curl-jq
    - terraform init
    - terraform validate
    - terraform fmt -recursive
  only:
    - branches
    - merge_requests

merge review:
  stage: plan
  script:
    - *install-curl-jq
    - terraform init
    - terraform plan -out=$PLAN
    - "terraform show --json $PLAN | convert_report > $JSON_PLAN_FILE"
  artifacts:
    expire_in: 1 week
    name: plan
    reports:
        terraform: $JSON_PLAN_FILE
  only:
    - merge_requests

plan production:
  stage: plan
  script:
    - *install-curl-jq
    - terraform init
    - terraform plan
  only:
    - main
  
apply:
  stage: apply
  script:
    - *install-curl-jq
    - terraform init
    - terraform apply -auto-approve
    - DYNAMIC_ENVIRONMENT_URL=$(terraform output -no-color)
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env
  dependencies: 
    - plan production
  artifacts:
    expire_in: 1 day
    name: $CI_COMMIT_REF_SLUG
    reports:
      dotenv: deploy.env
  # when: manual
  only:
    - main
  environment:
    name: production
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: destroy
    auto_stop_in: 3 hours

destroy:
  stage: destroy
  needs: []
  script:
    - *install-curl-jq
    - terraform init
    - terraform destroy -auto-approve
  when: manual
  only:
    - main  
  environment:
    name: production
    action: stop
